import org.apache.http.HttpHeaders;

import java.io.IOException;
import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Task {
    public static void main(String[] args) {
    task1();
    task2();
    }
    public static void task1() {
        HttpClient client = HttpClient.newHttpClient();
        HttpRequest request = HttpRequest.newBuilder()
                .GET()
                .uri(URI.create("https://www.opennet.ru/opennews/mini.shtml"))
                .build();
        String body = null;
        try {
            body = client.send(request, HttpResponse.BodyHandlers.ofString()).body();
        } catch (IOException | InterruptedException e) {
            e.printStackTrace();
        }

        Pattern pat = Pattern.compile("<td class=tdate>31\\.08\\.2021<\\/td>");
        int numberOfArticles = 0;
        if (body != null) {
            Matcher match = pat.matcher(body);
            while(match.find()) {
                numberOfArticles++;
            }
        }
        System.out.println("https://www.opennet.ru/opennews/mini.shtml site has " + numberOfArticles + " articles today");
    }

    public static void task2() {
        HttpClient client = HttpClient.newHttpClient();
        HttpRequest request = HttpRequest.newBuilder()
                .GET()
                .uri(URI.create("https://mirknig.su/"))
                .header(HttpHeaders.USER_AGENT, "Mozilla/5.0 Firefox/26.0")
                .setHeader("User-Agent", "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.11 (KHTML, like Gecko) Chrome/23.0.1271.95 Safari/537.11")
                .build();
        String body = null;
        try {
            body = client.send(request, HttpResponse.BodyHandlers.ofString()).body();
        } catch (IOException | InterruptedException e) {
            e.printStackTrace();
        }
        Pattern pat = Pattern.compile("Разместил\\: [<\\p{L}\\p{P}\\s\\d>=]+Сегодня,");

        int numberOfArticles = 0;
        if (body != null) {
            Matcher match = pat.matcher(body);
            while(match.find()) {
                numberOfArticles++;
            }
        }
        System.out.println("https://mirknig.su site has " + numberOfArticles + " articles today");
    }
}

